<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Flights;
use app\models\form\FlightDetailModel;
use app\models\form\FlightModel;
use app\models\form\FlightRouteModel;
use app\models\helpers\EWSHelper;
use app\models\Iport;
use Yii;
use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class FlightController extends Controller
{
    protected $prefixSSIM = 'PDCSKYUPYEAR';

    public function actionGetFlights()
    {
        $path = realpath(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web/ssim');
        $regex = '/^3\s(?<FltNo>PQ\s(\s\d{3}|\d{4}))\d{4}(?<STC>[\w])(?<DatOpFrom>\d{2}[\w]{3}\d{2})(?<DatOpTo>\d{2}[\w]{3}\d{2})(?<Days>[\s\d]{7})\s(?<DepStn>[\w]{3})(?<STD_UTC>\d{4})\d{4}[\+\-]\d{4}\s\s(?<ArrStn>[\w]{3})(?<STA_UTC>\d{4})\d{4}[\+\-]\d{4}/';
        $instance = new EWSHelper(Yii::$app->params['pdcmail']['host'], Yii::$app->params['pdcmail']['user'], Yii::$app->params['pdcmail']['pass']);
        $messageId = $instance->getLastMessageIdBySubject($this->prefixSSIM);
        $filePath = $instance->getMessageAttachment($messageId, $path);

        if ($filePath) {
            $lines = file_get_contents($filePath);
            $lines = explode("\n", $lines);

            foreach ($lines as $line) {

                if (preg_match($regex, $line, $matches)) {
                    $periodFrom = $this->getDate($matches['DatOpFrom']);
                    $periodTo = $this->getDate($matches['DatOpTo']);

                    $periodFromDayName = strtoupper(date('D', strtotime($periodFrom)));

                    $days = [];

                    for ($start = 0; $start <= 6; $start++) {
                        $index = $start + 1;
                        $days[$index] = substr($matches['Days'], $start, 1);
                    }

                    $std = substr($matches['STD_UTC'], 0, 2);
                    $std .= ':' . substr($matches['STD_UTC'], 2, 2);
                    $sta = substr($matches['STA_UTC'], 0, 2);
                    $sta .= ':' . substr($matches['STA_UTC'], 2, 2);

                    $condition = true;
                    $dateStart = $periodFrom;

                    while ($condition == true) {
                        foreach ($days as $day) {
                            if (empty(trim($day)))
                                continue;
                            $dayName = $this->getDay(trim($day));
                            $dateTime = new \DateTime($dateStart);
                            if ($periodFromDayName == $dayName) {
                                $date = $dateTime->format('Y-m-d');
                            } else {
                                $dateTime->modify('next ' . $dayName);
                                $date = $dateTime->format('Y-m-d');
                            }

                            if (strtotime($date) <= strtotime($periodTo)) {

                                if ($matches['STA_UTC'] < $matches['STD_UTC']) {
                                    $dateTime = new \DateTime($periodFrom);
                                    $dateTime->modify('tomorrow');
                                    $dateArrival = $dateTime->format('Y-m-d');
                                } else {
                                    $dateArrival = $date;
                                }

                                $model = new FlightModel();
                                $model->flightNumber = preg_replace('/ /', '', $matches['FltNo']);
                                $model->departureAirport = $matches['DepStn'];
                                $model->arrivalAirport = $matches['ArrStn'];
                                $model->departureTime = $date;
                                $model->departureStd = $std;
                                $model->arrivalTime = $dateArrival;
                                $model->arrivalSta = $sta;
                                FlightModel::saveFlight($model);
                            } else {
                                $condition = false;
                            }
                        }

                        $dateTime = new \DateTime($dateStart);
                        $dateTime->modify('+7 day');
                        $dateStart = $dateTime->format('Y-m-d');
                    }
                }
            }
            $date = date('Y-m-d H:i:s', strtotime('-1 day'));
            Flights::deleteAll(['<', 'updated_at', $date]);
            unlink($filePath);
        }
        return true;
    }

    public function actionGetPassengers($filter = true)
    {
        $list = FlightModel::getPassengers($filter);

        if (empty($list))
            return true;
        foreach ($list as $flight) {

            $schedule_id = null;

            if (empty($flight['flights_schedule_id'])) {
                //set key
                $params['key'] = Yii::$app->params['inventor_key'];
                $params['code'] = $flight['flight_number'];
                $params['date'] = date('Y-m-d', strtotime($flight['departure_time']));
                $params['utc'] = '1';
                $requestUrl = Yii::$app->params['inventor_url'] . Yii::$app->params['inventor_version'] . '/flights/get-flight' . '?' . http_build_query($params);

                $number = FlightDetailModel::getRegNumber($requestUrl, $params);
                if ($number) {
                    $model = FlightModel::getFlightByID($flight['id']);
                    $model->flightScheduleId = $number;
                    FlightModel::saveFlight($model);
                    $schedule_id = $number;
                }
            } else {
                $schedule_id = $flight['flights_schedule_id'];
            }

            $params = [
                'key' => Yii::$app->params['inventor_key'],
                'schedule_id' => $schedule_id
            ];

            $pnlData = FlightDetailModel::getPnl($params);

            if ($pnlData["response"]["result"]["code"] != "0" || empty($pnlData["response"]["PassengerNameList"])) {
                continue;
            };
            $total = [
                'male' => 0,
                'female' => 0,
                'child' => 0,
                'infant' => 0,
            ];
            foreach ($pnlData["response"]["PassengerNameList"] as $passengers) {

                switch ($passengers['passenger']['gender']) {
                    case 'male':
                        $total['male'] += 1;
                        break;
                    case 'female':
                        $total['female'] += 1;
                        break;
                    case 'child':
                        $total['child'] += 1;
                        break;
                    case 'infant':
                        $total['infant'] += 1;
                        break;
                }
            }

            $flightRoute = FlightRouteModel::getFlightByRoute(['from'=>$flight['departure_airport'], 'to'=>$flight['arrival_airport']]);

            $flightInfo = new FlightDetailModel();
            $flightInfo->flightId = $flight['id'];
            $flightInfo->totalMale = $total['male'];
            $flightInfo->totalFemale = $total['female'];
            $flightInfo->totalChild = $total['child'];
            $flightInfo->totalInfant = $total['infant'];
            if($flightRoute) {
                $flightInfo->averageBaggageWeight = $flightRoute->averageBaggageWeight;
                $flightInfo->noShow = $flightRoute->noShow;
            }
            FlightDetailModel::saveFlight($flightInfo);
        }
        return true;
    }

    public function actionCargoWeight()
    {
        $regex = '/^3\s(?<FltNo>PQ\s(\s\d{3}|\d{4}))\d{4}(?<STC>[\w])(?<DatOpFrom>\d{2}[\w]{3}\d{2})(?<DatOpTo>\d{2}[\w]{3}\d{2})(?<Days>[\s\d]{7})\s(?<DepStn>[\w]{3})(?<STD_UTC>\d{4})\d{4}[\+\-]\d{4}\s\s(?<ArrStn>[\w]{3})(?<STA_UTC>\d{4})\d{4}[\+\-]\d{4}/';
        $path = realpath(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web/ssim');
        $instance = new EWSHelper(Yii::$app->params['cargo_mail']['host'], Yii::$app->params['cargo_mail']['user'], Yii::$app->params['cargo_mail']['pass']);
        $messageId = $instance->getLastMessageIdBySubject($this->prefixSSIM);
        $filePath = $instance->getMessageAttachment($messageId, $path);

        if ($filePath) {
            $lines = file_get_contents($filePath);
            $lines = explode("\n", $lines);

            foreach ($lines as $line) {

                if (preg_match($regex, $line, $matches)) {
                    $periodFrom = $this->getDate($matches['DatOpFrom']);
                    $periodTo = $this->getDate($matches['DatOpTo']);

                    $periodFromDayName = strtoupper(date('D', strtotime($periodFrom)));

                    $days = [];

                    for ($start = 0; $start <= 6; $start++) {
                        $index = $start + 1;
                        $days[$index] = substr($matches['Days'], $start, 1);
                    }

                    $std = substr($matches['STD_UTC'], 0, 2);
                    $std .= ':' . substr($matches['STD_UTC'], 2, 2);
                    $sta = substr($matches['STA_UTC'], 0, 2);
                    $sta .= ':' . substr($matches['STA_UTC'], 2, 2);

                    $condition = true;
                    $dateStart = $periodFrom;

                    while ($condition == true) {
                        foreach ($days as $day) {
                            if (empty(trim($day)))
                                continue;
                            $dayName = $this->getDay(trim($day));
                            $dateTime = new \DateTime($dateStart);
                            if ($periodFromDayName == $dayName) {
                                $date = $dateTime->format('Y-m-d');
                            } else {
                                $dateTime->modify('next ' . $dayName);
                                $date = $dateTime->format('Y-m-d');
                            }

                            if (strtotime($date) <= strtotime($periodTo)) {

                                if ($matches['STA_UTC'] < $matches['STD_UTC']) {
                                    $dateTime = new \DateTime($periodFrom);
                                    $dateTime->modify('tomorrow');
                                    $dateArrival = $dateTime->format('Y-m-d');
                                } else {
                                    $dateArrival = $date;
                                }

                                $model = new FlightModel();
                                $model->flightNumber = preg_replace('/ /', '', $matches['FltNo']);
                                $model->departureAirport = $matches['DepStn'];
                                $model->arrivalAirport = $matches['ArrStn'];
                                $model->departureTime = $date;
                                $model->departureStd = $std;
                                $model->arrivalTime = $dateArrival;
                                $model->arrivalSta = $sta;
                                FlightModel::saveFlight($model);
                            } else {
                                $condition = false;
                            }
                        }

                        $dateTime = new \DateTime($dateStart);
                        $dateTime->modify('+7 day');
                        $dateStart = $dateTime->format('Y-m-d');
                    }
                }
            }
            $date = date('Y-m-d H:i:s', strtotime('-1 day'));
            Flights::deleteAll(['<', 'updated_at', $date]);
        }
        echo $filePath;
        return true;
    }

    protected function getDate($date)
    {
        $day = substr($date, 0, 2);
        $month = $this->getMonth(substr($date, 2, 3))[0];
        $year = substr($date, 5, 2);
        $result = '20' . $year . '-' . $month . '-' . $day;

        return $result;
    }

    public function getMonth($name)
    {
        $list = [
            '01' => 'JAN',
            '02' => 'FEB',
            '03' => 'MAR',
            '04' => 'APR',
            '05' => 'MAY',
            '06' => 'JUN',
            '07' => 'JUL',
            '08' => 'AUG',
            '09' => 'SEP',
            '10' => 'OCT',
            '11' => 'NOV',
            '12' => 'DEC',
        ];
        return array_keys($list, $name);
    }

    public function getDay($number)
    {
        $list = [
            1 => 'MON',
            2 => 'TUE',
            3 => 'WED',
            4 => 'THU',
            5 => 'FRI',
            6 => 'SAT',
            7 => 'SUN',
        ];
        return $list[$number];
    }

}
