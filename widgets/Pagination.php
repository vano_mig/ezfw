<?php
/**
 * Pagination PHP widget
 *
 * @copyright SkyUp Airlines 2020
 * @date 17.03.2020
 * @author apocat
 */

namespace app\widgets;

use \yii\bootstrap4\Widget;

class Pagination extends Widget
{
    public $joiner;
    public $page;
    public $pages;
    public $inout;
    public $leftright;
    public $baseUrl;
    public $className;
    public $decorator;
    public $generator;

    /**
     * Декоратор ссылок навигации
     *
     * @param string $sLink - ссылка страницы
     * @param string $sClassName - класс ссылки
     *
     * @return mixed|string
     */
    private function decorate($sLink = '', $sClassName = '')
    {
        if ($this->decorator instanceof \Closure)
            return call_user_func($this->decorator, $sLink, $sClassName);

        return '<li' . (($sClassName == '') ? '' : ' class="' . $sClassName . '"') . '>' . $sLink . '</li>';
    }

    /**
     * Генератор ссылок навигации
     *
     * @param int $nPage - номер страницы
     *
     * @return mixed|string
     */
    private function generate($nPage = 1)
    {
        if ($this->generator instanceof \Closure) {
            $sResult = call_user_func($this->generator, $nPage);
            return is_string($sResult) ? $this->baseUrl . $sResult : $sResult;
        }

        return $this->baseUrl . '?page=' . $nPage;
    }

    /**
     * Инициализация виджета
     */
    public function init()
    {
        parent::init();

        $view = $this->getView();
        $view->registerCssFile('/css/pagination.css?v=' . microtime());
//        $view->registerJsFile('/js/pagination.js?v=' . microtime());

        if ($this->joiner === null) $this->joiner = PHP_EOL;
        if ($this->page === null) $this->page = 1;
        if ($this->pages === null) $this->pages = 1;
        if ($this->inout === null) $this->inout = 3;
        if ($this->leftright === null) $this->leftright = 5;
        if ($this->baseUrl === null) $this->baseUrl = '';
        if ($this->className === null) $this->className = 'pagination';
    }

    /**
     * Выполнение виджета
     *
     * @return string
     */
    public function run()
    {
        $arrItems = [];
        $nPagePrev = 0;

        for ($nPage = 1; $nPage <= $this->pages; $nPage++) {
            $bShow = false;

            // Первые обязательные ссылки
            if (($nPage >= 1) && ($nPage <= $this->inout)) $bShow = true;

            // Последние обязательные ссылки
            if ($nPage > ($this->pages - $this->inout)) $bShow = true;

            if ($this->page >= ($nPage - $this->leftright) && $this->page <= ($nPage + $this->leftright))
                $bShow = true;

            if (!empty($bShow)) {
                if ($nPage - 1 != $nPagePrev) $arrItems[] = $this->decorate('...', 'more');

                $bActive = $this->page == $nPage;
                $sLink = $this->generate($nPage);

                if (is_string($sLink))
                    $sLink = '<a href="' . $sLink . '"' . (($bActive) ? ' class="active"' : '') . '>' . $nPage . '</a>';
                else
                    $sLink = '<a' . (($bActive) ? ' class="active"' : '') . '>' . $nPage . '</a>';

                $arrItems[] = $this->decorate($sLink, (($bActive) ? 'active' : ''));

                $nPagePrev = $nPage;
            }
        }

        return '<ul class="' . $this->className . '">' . implode($this->joiner, $arrItems) . '</ul>';
    }
}