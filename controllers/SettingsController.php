<?php

namespace app\controllers;

use app\models\Auth;
use app\models\FlightsRoutes;
use app\models\form\FlightModel;
use app\models\form\FlightRouteModel;
use app\models\form\UserForm;
use app\models\User;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class SettingsController extends Controller
{
    public $layout = 'main';

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionShow()
    {

        $user = Auth::getLoggedUser();
        if (!$user)
            return $this->redirect('/login');

        $bIsAjax = Yii::$app->request->getIsAjax();

        $page = (int)Yii::$app->request->get('page', 1);
        $search = Yii::$app->request->get('search', []);

        $arrFilter = [
            'page' => $page,
            'search' => $search,
        ];

        $limit = 10;
        $offset = $page * $limit;
        $offset = $offset - $limit;

        $totalCount = FlightModel::getFlightList($search, $limit, $offset, true);
        $flightsList = FlightModel::getFlightList($search, $limit, $offset);

        $allPages = ceil($totalCount / $limit);

        if (empty($bIsAjax)) {
            return $this->render("show", [
                'pageCount' => $totalCount,
                'flights' => $flightsList,
                'search' => $search,
                'filter' => $arrFilter,
                'pagination' => [
                    'currentPage' => $page,
                    'pages' => $allPages
                ]
            ]);
        } else {
            return $this->renderPartial("show", [
                'pageCount' => $totalCount,
                'flights' => $flightsList,
                'search' => $search,
                'filter' => $arrFilter,
                'pagination' => [
                    'currentPage' => $page,
                    'pages' => $allPages
                ]
            ]);
        }
    }

    /**
     * Displays User model list.
     *
     * @return string
     */
    public function actionUsers()
    {
        $user = Auth::getLoggedUser();
        if (!$user)
            return $this->redirect('/login');

        $bIsAjax = Yii::$app->request->getIsAjax();

        $page = (int)Yii::$app->request->get('page', 1);
        $search = (int)Yii::$app->request->get('search', []);

        $arrFilter = [
            'page' => $page,
            'search' => $search,
        ];

        $limit = 10;
        $offset = $page * $limit;
        $offset = $offset - $limit;

        $totalCount = User::getList($limit, $offset, true);
        $userList = User::getList($limit, $offset);

        $allPages = ceil($totalCount / $limit);

        if (empty($bIsAjax)) {
            return $this->render("index", [
                'pageCount' => $totalCount,
                'userList' => $userList,
                'search' => $search,
                'filter' => $arrFilter,
                'pagination' => [
                    'currentPage' => $page,
                    'pages' => $allPages
                ]
            ]);
        } else {
            return $this->renderPartial("show", [
                'pageCount' => $totalCount,
                'userList' => $userList,
                'search' => $search,
                'filter' => $arrFilter,
                'pagination' => [
                    'currentPage' => $page,
                    'pages' => $allPages
                ]
            ]);
        }
    }

    /**
     * Display new user form.
     *
     * @return string
     */
    public function actionCreate()
    {
        $user = Auth::getLoggedUser();
        if (!$user)
            return $this->redirect('/login');
        $model = new UserForm();
        $model->scenario = 'insert';
        return $this->render('new_user', ['user' => $model]);
    }

    public function actionSaveUser()
    {
        if (Yii::$app->request->post('UserForm')) {

            $model = new UserForm();
            $model->attributeForm(Yii::$app->request->post('UserForm'));

            if ($model->validateFrom() && User::saveUser($model)) {
                Yii::$app->session->setFlash('success', 'Record was successfully saved');
                return $this->redirect('/users');
            }
            $view = 'new_user';
            if ($model->userId)
                $view = 'update_user';

            return $this->render($view, ['user' => $model]);
        }
    }

    /**
     * Display new user form.
     *
     * @return string
     */
    public function actionUpdateUser()
    {
        $id = Yii::$app->request->get('id');
        $user = Auth::getLoggedUser();
        if (!$user)
            return $this->redirect('/login');
        $model = User::getUserByUserId($id);
        $model->scenario = 'update';
        return $this->render('update_user', ['user' => $model]);
    }

    public function actionNoShowBase()
    {
        $user = Auth::getLoggedUser();
        if (!$user)
            return $this->redirect('/login');

        $bIsAjax = Yii::$app->request->getIsAjax();

        $page = (int)Yii::$app->request->get('page', 1);
        $search = (int)Yii::$app->request->get('search', []);

        $arrFilter = [
            'page' => $page,
            'search' => $search,
        ];

        $limit = 10;
        $offset = $page * $limit;
        $offset = $offset - $limit;

        $totalCount = FlightsRoutes::getList($limit, $offset, true);
        $routeList = FlightsRoutes::getList($limit, $offset);

        $allPages = ceil($totalCount / $limit);

        if (empty($bIsAjax)) {
            return $this->render("no_show", [
                'pageCount' => $totalCount,
                'routeList' => $routeList,
                'search' => $search,
                'filter' => $arrFilter,
                'pagination' => [
                    'currentPage' => $page,
                    'pages' => $allPages
                ]
            ]);
        } else {
            return $this->renderPartial("no_show", [
                'pageCount' => $totalCount,
                'routeList' => $routeList,
                'search' => $search,
                'filter' => $arrFilter,
                'pagination' => [
                    'currentPage' => $page,
                    'pages' => $allPages
                ]
            ]);
        }
    }

    public function actionNewRoute()
    {
        $user = Auth::getLoggedUser();
        if (!$user)
            return $this->redirect('/login');
        $model = new FlightRouteModel();
        return $this->render('route_form', ['route' => $model]);
    }

    public function actionSaveRoute()
    {
        if (Yii::$app->request->post('FlightRouteModel')) {

            $model = new FlightRouteModel();
            $model->attributeForm(Yii::$app->request->post('FlightRouteModel'));

            if ($model->validateFrom() && FlightRouteModel::saveRoute($model)) {
                Yii::$app->session->setFlash('success', 'Record was successfully saved');
                return $this->redirect('/no-show-base');
            }
            if ($model->id)
                $view = 'update_route_form';
            else
                $view = 'route_form';

            return $this->render($view, ['route' => $model]);
        }
    }

    public function actionUpdateRoute($id)
    {
        $user = Auth::getLoggedUser();
        if (!$user)
            return $this->redirect('/login');
        $model = FlightRouteModel::getRouteByID($id);
        return $this->render('update_route_form', ['route' => $model]);
    }

    public function actionDeleteRoute($id)
    {
        $user = Auth::getLoggedUser();
        if (!$user)
            return $this->redirect('/login');
       FlightsRoutes::find()->where(['id'=>$id])->delete();
       return $this->redirect('/no-show-base');
    }
}
