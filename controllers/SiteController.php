<?php

namespace app\controllers;

use app\models\Auth;
use app\models\User;
use app\models\UserLogbook;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

class SiteController extends Controller
{
    public $layout = 'auth';
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return Yii::$app->response->redirect('/login');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $request = \Yii::$app->request;

        if ($request->post('email') && $request->post('password')) {
            if ( Auth::login($request->post('email'), $request->post('password')) ) {
                UserLogbook::log("Authorization, email `{$request->post('email')}`");
                return $this->redirect('/show');
            } else {
                return $this->render("login", ['error' => true]);
            }
        } else {
            return $this->render("login", [ 'error' => false ]);
        }

    }

    public function actionLogout()
    {
        Auth::logout();
        return $this->redirect('/');
    }

    public function actionForgotPassword()
    {
        //check if user currently logged
        if (Auth::getLoggedUser()) {
            return Yii::$app->response->redirect('/show');
        }

        if(Yii::$app->request->post()) {
            $loginForm = Yii::$app->request->post('LoginForm', false);

            if (!$loginForm) {
                return $this->render('forgot', ['status' => false]);
            }

            $user = User::getUserByEmail($loginForm['email']);
            if (!$user) {
                return $this->render('forgot', ['status' => false]);
            }
            //set new password
            $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
            $count = mb_strlen($chars);
            $newPasswd = '';

            for ($i = 0; $i < 6; $i++) {
                $index = rand(0, $count - 1);
                $newPasswd .= mb_substr($chars, $index, 1);
            }

            $user->password = Yii::$app->getSecurity()->generatePasswordHash($newPasswd);
            $user->save();
            UserLogbook::log("Forgot password, user `{$user->id}` name `{$user->name}` email `{$user->email}`");

            Yii::$app->mailer->compose('password-reset', ['user' => $user, 'password' => $newPasswd])
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['supportEmailUserName']])
                ->setTo($user->email)
                ->setReplyTo('agent@skyup.aero')
                ->setSubject("SkyUp Agent password reset")
                ->send();
            return $this->render('forgot', ['status' => true]);
        }


        return $this->render('forgot');
    }
}
