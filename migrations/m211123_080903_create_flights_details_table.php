<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%flights_details}}`.
 */
class m211123_080903_create_flights_details_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%flights_details}}', [
            'id' => $this->bigPrimaryKey(),
            'flight_id'=>$this->integer()->null()->defaultValue(null),
            'total_male'=>$this->integer()->null()->defaultValue(null),
            'total_female'=>$this->integer()->null()->defaultValue(null),
            'total_child'=>$this->integer()->null()->defaultValue(null),
            'total_infant'=>$this->integer()->null()->defaultValue(null),
            'average_baggage_weight'=>$this->decimal(10,2)->null()->defaultValue(null),
            'cargo_weight'=>$this->decimal(10,2)->null()->defaultValue(null),
            'no_show'=>$this->decimal(10,2)->null()->defaultValue(null),
            'czwf'=>$this->decimal(10,2)->null()->defaultValue(null),
            'ezwf'=>$this->decimal(10,2)->null()->defaultValue(null),
            'dow'=>$this->decimal(10,2)->null()->defaultValue(null),
            'azwf'=>$this->decimal(10,2)->null()->defaultValue(null),
            'note'=>$this->string()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%flights_details}}');
    }
}
