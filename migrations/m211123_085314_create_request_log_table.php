<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%request_log}}`.
 */
class m211123_085314_create_request_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%request_log}}', [
            'id' => $this->primaryKey(),
            'request_url'=>$this->text()->null()->defaultValue(null),
            'request_params'=>$this->text()->null()->defaultValue(null),
            'response'=>$this->text()->null()->defaultValue(null),
            'created_at'=>$this->dateTime()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%request_log}}');
    }
}
