<?php

use app\models\form\UserForm;
use app\models\User;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 */
class m211115_114139_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("users",[
            'id' => $this->primaryKey(),
            'email' => $this->string()->null()->defaultValue(null),
            'name' => $this->string()->null()->defaultValue(null),
            'surname' => $this->string()->null()->defaultValue(null),
            'password' => $this->string()->null()->defaultValue(null),
            'password_reset' => $this->string()->null()->defaultValue(null),
            'company' => $this->string()->null()->defaultValue(null),
            'active' => $this->tinyInteger(1)->null()->defaultValue(null),
            'is_admin'=> "ENUM('N', 'Y') DEFAULT 'N'",
            'created_at'=>$this->dateTime()->null()->defaultValue(null),
            'updated_at'=>$this->dateTime()->null()->defaultValue(null),
        ]);

        $user = new User();
        $user->name = 'admin';
        $user->surname = 'admin';
        $user->email = 'admin@ezfw.aero';
        $user->password = Yii::$app->getSecurity()->generatePasswordHash('vrawlsfd2134sd');
        $user->active = 1;
        $user->is_admin = 'Y';

        $user->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%users}}');
    }
}
