<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%flights}}`.
 */
class m211118_142219_create_flights_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%flights}}', [
            'id' => $this->bigPrimaryKey(),
            'flight_number'=>$this->string()->null()->defaultValue(null),
            'reg_number'=>$this->string()->null()->defaultValue(null),
            'departure_airport'=>$this->string()->null()->defaultValue(null),
            'departure_time'=>$this->dateTime()->null()->defaultValue(null),
            'departure_std'=>$this->string()->null()->defaultValue(null),
            'arrival_airport'=>$this->string()->null()->defaultValue(null),
            'arrival_time'=>$this->dateTime()->null()->defaultValue(null),
            'arrival_sta'=>$this->string()->null()->defaultValue(null),
            'created_at'=>$this->dateTime()->null()->defaultValue(null),
            'updated_at'=>$this->dateTime()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%flights}}');
    }
}
