<?php

use yii\db\Migration;

/**
 * Class m211129_144414_add_field_to_flights
 */
class m211129_144414_add_field_to_flights extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%flights}}', 'flights_schedule_id', $this->integer()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%flights}}', 'flights_schedule_id');
    }
}
