<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%flights_routes}}`.
 */
class m211126_074059_create_flights_routes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%flights_routes}}', [
            'id' => $this->primaryKey(),
            'from' => $this->string(3)->null()->defaultValue(null),
            'to' => $this->string(3)->null()->defaultValue(null),
            'average_baggage_weight' => $this->decimal(10,2)->null()->defaultValue(null),
            'no_show' => $this->decimal(10,2)->null()->defaultValue(null),
            'note' => $this->string()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%flights_routes}}');
    }
}
