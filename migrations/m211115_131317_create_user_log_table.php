<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_log}}`.
 */
class m211115_131317_create_user_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_logbook}}', [
            'id' => $this->primaryKey(),
            'email' => $this->string(),
            'action' => $this->string(),
            'timestamp' => $this->string()
        ]);
        $this->createIndex('email', 'user_logbook', 'email');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_logbook}}');
    }
}
