/**
 * Pagination JS
 *
 * @copyright SkyUp Airlines 2020
 * @date 18.03.2020
 * @author apocat
 */

$(document).ready(function() {

	let pagination = {

		instance: null,
		selectors: {
			navigation: 'ul.pagination',
			content: '#pagination',
			filter: 'form.pagination'
		},
		paginationName: null,
		loadingClass: 'loading',
		currentLink: null,
		minValue: 1,
		maxValue: 1,
		currentValue: 1,
		arrayJoiner: '-',

		/**
		 * Инициализация мягкой навигации
		 *
		 * @param reinit - повторная инициализация
		 */
		init: function(reinit) {
			this.instance = $(this.selectors.navigation);

			$('a', this.instance).off('click.pagination').on('click.pagination', this.onClick);

			// Внешняя перезагрузка и установка параметров и отправка формы поиска
			this.instance.on('reload', this.externalReload);
			this.instance.on('submit', this.externalSubmit);

			// Перехват нативного события отправки формы
			$(this.selectors.filter).each(function() {
				$(this).off('submit.pagination').on('submit.pagination', pagination.onFilterSubmit);
			});

			// Запоминаем текущие значения выбранной страницы
			this.scanMinMaxValues();

			if (!reinit) {
				let paginationName = $(this.selectors.content).attr('rel');

				this.paginationName = (typeof paginationName === undefined) ? 'pagination' : paginationName;
				this.currentLink = location.href;

				$(window).bind('popstate', this.onHistory);
				$(document).on('keydown', this.binds)
			}
		},

		/**
		 * Обработчик отправки формы
		 */
		onFilterSubmit: function(e, page) {
			let params = pagination.getParameters(false);

			if (page) params['page'] = page; else delete params['page'];

			$('select, input', $(this)).each(function() {
				let name = $(this).attr('name');
				let value = $(this).val();
				let type = $(this).attr('type');

				if (name)
					if ((value === '') || (value === null))
						delete params[name];
					else {
						switch (type) {
							default:
								value = (typeof value === 'object')
									? value.join(pagination.arrayJoiner)
									: value;

								if (value === '')
									delete params[name];
								else
									params[name] = value;
								break;
							case 'checkbox':
								if ($(this).prop('checked'))
									params[name] = $(this).val();
								else
									delete params[name];
								break;
						}
					}
			});

			$('<a/>').attr('href', '?' + $.param(params)).on('click', pagination.onClick).trigger('click');

			return false;
		},

		/**
		 * Перехват всех нажатий на ссылки навигации
		 *
		 * @param e - обьект события нажатия на ссылку
		 */
		onClick: function(e) {
			e.preventDefault();

			let href = $(this).attr('href').toString();

			// Загрузка данных по ссылке
			pagination.getData(href, function(data) {
				if (window.location.search !== href) history.pushState({page: href}, null, href);

				pagination.setContent(data);
			});
		},

		/**
		 * Обработка навигации по истории
		 */
		onHistory: function() {
			// Загрузка данных по ссылке из истории
			pagination.getData(window.location.href, function(data) {
				pagination.setContent(data);
			});
		},

		/**
		 * Запрос на сервер за данными
		 *
		 * @param href - ссылка
		 * @param callback - обратный вызов
		 */
		getData: function(href, callback) {
			window.skySocket.ajaxQueue(pagination.paginationName, {
				type: 'get',
				url: href,
				dataType: 'html',
				beforeSend: function() {
					$('#aloader').show();
					$(pagination.selectors.content).addClass(pagination.loadingClass);
				},
				complete: function(e) {
					let redirect = e.getResponseHeader('X-Redirect');

					if (redirect)
						window.location.href = redirect;
					else {
						$('#aloader').hide();
						$(pagination.selectors.content).removeClass(pagination.loadingClass);
					}
				},
				success: function(data) {
					(callback) && callback(data);
				}
			});
		},

		/**
		 * Получаем параметры из строки
		 *
		 * @param string - строка (если задана)
		 *
		 * @returns {{}}
		 */
		getParameters: function(string) {
			let match,
				regular = /\+/g,
				search = /([^&=]+)=?([^&]*)/g,
				decode = function(value) {
					value = value.replace(regular, ' ');
					try {
						value = decodeURIComponent(value);
					} catch (e) {
						console.warn(e.name + '<br>' + e.message);
					}
					return value;
				},
				query = (string) ? string : window.location.search.substring(1);

			let urlParams = {};

			while (match = search.exec(query))
				urlParams[decode(match[1])] = decode(match[2]);

			return urlParams;
		},

		/**
		 * Отображение полученных данных
		 *
		 * @param data - код документ
		 */
		setContent: function(data) {
			$('html, body').stop().animate({scrollTop: 0}, 200, 'swing');

			$(pagination.selectors.content).after(data).remove();
			pagination.init(true);
		},

		/**
		 * Внешний вызов перезагрузки данных
		 */
		externalReload: function() {
			let params = pagination.getParameters(false);
			let page = params.hasOwnProperty('page') ? params['page'] : false;

			// Отправляем форму поиска
			pagination.onFilterSubmit.bind($(pagination.selectors.filter), true)(page);
		},

		/**
		 * Внешняя отправка формы с параметрами или без
		 *
		 * @param e - событие
		 * @param params - параметры
		 */
		externalSubmit: function(e, params) {
			if (params)
				for (let key in params)
					$('[name="' + key + '"]', $(pagination.selectors.filter)).val(params[key]);

			// Отправляем форму поиска
			pagination.onFilterSubmit.bind($(pagination.selectors.filter))();
		},

		/**
		 * События на странице с клавишами
		 *
		 * @param e - событие
		 */
		binds: function(e) {
			if (e.metaKey || e.ctrlKey) {
				let navigate = false;
				let params = pagination.getParameters(false);

				switch (e.keyCode) {
					case 37: // влево
						if (pagination.currentValue > pagination.minValue) {
							params['page'] = pagination.currentValue - 1;
							navigate = $.param(params);
						}
						break;
					case 38: // вверх
						params['page'] = pagination.maxValue;
						navigate = $.param(params);
						break;
					case 39: // вправо
						if (pagination.currentValue < pagination.maxValue) {
							params['page'] = pagination.currentValue + 1;
							navigate = $.param(params);
						}
						break;
					case 40: // вниз
						params['page'] = 1;
						navigate = $.param(params);
						break;
				}

				if (navigate)
					$('<a/>').attr('href', '?' + $.param(params)).on('click', pagination.onClick).trigger('click');
			}
		},

		/**
		 * Сканируем навигатор на текущие значениея страницы
		 */
		scanMinMaxValues: function() {
			let params = pagination.getParameters(false);

			pagination.minValue = $('li:first-child', pagination.instance).text() * 1;
			pagination.maxValue = $('li:last-child', pagination.instance).text() * 1;
			pagination.currentValue = params.hasOwnProperty('page') ? params['page'] * 1 : 1;
		}
	};

	pagination.init();
});
