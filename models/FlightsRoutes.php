<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\db\Query;

class FlightsRoutes extends ActiveRecord
{
    public static function tableName()
    {
        return 'flights_routes';
    }

    public static function getModelById($id)
    {
        return self::findOne($id);
    }
    public static function getModelByRoute($from, $to)
    {
        return self::findOne(['from'=>$from, 'to'=>$to]);
    }

    public static function getList($limit = false, $offset = false, $count = false)
    {
        $models = self::find()->orderBy(['id'=>SORT_DESC]);
        if ($limit !== false && $offset !== false) {
            $models->limit($limit);
            $models->offset($offset);
        }
        if($count)
            return $models->count();

        return $models->all();
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $flights = Flights::find()->where(['departure_airport'=>$this->from, 'arrival_airport'=>$this->airport])->all();
        if(!empty($flights)) {
            $items = [];
            foreach ($flights as $flight) {
                $items[] =$flight->id;
            }
            FlightDetails::updateAll(['average_baggage_weight'=>$this->average_baggage_weight, 'no_show'=>$this->no_show], ['id'=>$items]);
        }
        return true;
    }
}
