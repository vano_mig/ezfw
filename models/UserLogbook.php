<?php
/**
 * Created by PhpStorm.
 * User: yurypatrin
 * Date: 2019-04-10
 * Time: 17:51
 */

namespace app\models;


use yii\db\ActiveRecord;

class UserLogbook extends ActiveRecord
{
    public static function log($action)
    {
        $model = new UserLogbook();
        $model->action = $action;
        $model->timestamp = time();

        $user = Auth::getLoggedUser();
        if ($user) $model->email = $user->email;

        $model->save();
    }
}