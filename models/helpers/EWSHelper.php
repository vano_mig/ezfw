<?php

namespace app\models\helpers;

use DateTime;
use Exception;
use Generator;
use jamesiarmes\PhpEws\Client;
use \jamesiarmes\PhpEws\Request\FindItemType;
use \jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfBaseFolderIdsType;
use jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfBaseItemIdsType;
use jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfRequestAttachmentIdsType;
use \jamesiarmes\PhpEws\Enumeration\DefaultShapeNamesType;
use \jamesiarmes\PhpEws\Enumeration\DistinguishedFolderIdNameType;
use \jamesiarmes\PhpEws\Enumeration\ResponseClassType;
use \jamesiarmes\PhpEws\Enumeration\UnindexedFieldURIType;
use jamesiarmes\PhpEws\Request\GetAttachmentType;
use jamesiarmes\PhpEws\Request\GetItemType;
use jamesiarmes\PhpEws\Response\FindItemResponseType;
use \jamesiarmes\PhpEws\Type\AndType;
use \jamesiarmes\PhpEws\Type\ConstantValueType;
use \jamesiarmes\PhpEws\Type\DistinguishedFolderIdType;
use \jamesiarmes\PhpEws\Type\FieldURIOrConstantType;
use \jamesiarmes\PhpEws\Type\IsGreaterThanOrEqualToType;
use \jamesiarmes\PhpEws\Type\IsLessThanOrEqualToType;
use jamesiarmes\PhpEws\Type\ItemIdType;
use \jamesiarmes\PhpEws\Type\ItemResponseShapeType;
use \jamesiarmes\PhpEws\Type\PathToUnindexedFieldType;
use jamesiarmes\PhpEws\Type\RequestAttachmentIdType;
use \jamesiarmes\PhpEws\Type\RestrictionType;
use Yii;

class EWSHelper
{
    private $client;

    function __construct(string $host, string $user, string $pass)
    {
        $this->client = new Client($host, $user, $pass);
        $this->client->setCurlOptions([
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLINFO_HEADER_OUT => true,
        ]);
        $this->client->setTimezone('UTC');
    }

    public function getLastMessageId(): string
    {
        $response = $this->getRequestBase();

        $response_messages = $response->ResponseMessages->FindItemResponseMessage;
        foreach ($response_messages as $response_message) {
            if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {
                $code = $response_message->ResponseCode;
                $message = $response_message->MessageText;
                Yii::error("Failed to search for messages with \"$code: $message\"");
                continue;
            }

            $items = $response_message->RootFolder->Items->Message;
            foreach ($items as $item) {
                return $item->ItemId->Id;
            }
        }
    }


    public function getLastMessageIdBySubject(string $subject): string
    {
        $response = $this->getRequestBase();

        $response_messages = $response->ResponseMessages->FindItemResponseMessage;
        foreach ($response_messages as $response_message) {
            if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {
                $code = $response_message->ResponseCode;
                $message = $response_message->MessageText;
                Yii::error("Failed to search for messages with \"$code: $message\"");
                continue;
            }

            $items = $response_message->RootFolder->Items->Message;
            foreach ($items as $item) {
                if (strpos($item->Subject, $subject) === 0) {
                    return $item->ItemId->Id;
                }
            }
        }
        return '';
    }

    public function getMessageAttachment(string $messageId, string $path)
    {
        $request = new GetItemType();
        $request->ItemShape = new ItemResponseShapeType();
        $request->ItemShape->BaseShape = DefaultShapeNamesType::ALL_PROPERTIES;
        $request->ItemIds = new NonEmptyArrayOfBaseItemIdsType();

        // Add the message id to the request.
        $item = new ItemIdType();
        $item->Id = $messageId;
        $request->ItemIds->ItemId[] = $item;

        $response = $this->client->GetItem($request);

        // Iterate over the results, printing any error messages or receiving
        // attachments.
        $response_messages = $response->ResponseMessages->GetItemResponseMessage;
        foreach ($response_messages as $response_message) {
            // Make sure the request succeeded.
            if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {
                $code = $response_message->ResponseCode;
                $message = $response_message->MessageText;
                Yii::error("Failed to get message with \"$code: $message\"");
                continue;
            }

            // Iterate over the messages, getting the attachments for each.
            $attachments = array();
            foreach ($response_message->Items->Message as $item) {
                // If there are no attachments for the item, move on to the next
                // message.
                if (empty($item->Attachments)) {
                    continue;
                }

                // Iterate over the attachments for the message.
                foreach ($item->Attachments->FileAttachment as $attachment) {
                    $attachments[] = $attachment->AttachmentId->Id;
                }
            }

            // Build the request to get the attachments.
            $request = new GetAttachmentType();
            $request->AttachmentIds = new NonEmptyArrayOfRequestAttachmentIdsType();

            // Iterate over the attachments for the message.
            foreach ($attachments as $attachment_id) {
                $id = new RequestAttachmentIdType();
                $id->Id = $attachment_id;
                $request->AttachmentIds->AttachmentId[] = $id;
            }

            $response = $this->client->GetAttachment($request);

            // Iterate over the response messages, printing any error messages or
            // saving the attachments.
            $attachment_response_messages = $response->ResponseMessages
                ->GetAttachmentResponseMessage;
            foreach ($attachment_response_messages as $attachment_response_message) {
                // Make sure the request succeeded.
                if ($attachment_response_message->ResponseClass != ResponseClassType::SUCCESS) {
                    $code = $response_message->ResponseCode;
                    $message = $response_message->MessageText;
                    Yii::error("Failed to get attachment with \"$code: $message\"");
                    continue;
                }

                // Iterate over the file attachments, saving each one.
                $attachments = $attachment_response_message->Attachments->FileAttachment;
                foreach ($attachments as $attachment) {
                    $path = "$path/pdc_sync_" . date('YmdHis') . "_" . str_replace(['/', '\\', chr(0)], '', $attachment->Name);
                    file_put_contents($path, $attachment->Content);
                    return $path;
                }
            }
        }
    }

    private function getRequestBase($from = null, $to = null): FindItemResponseType
    {
        $date = new DateTime(date('Y-m-d H:i:s'));
        $dateStart = new DateTime(date('Y-m-d H:i:s', strtotime("-1 year")));
        $date->setTimezone(new \DateTimeZone('UTC'));
        $dateStart->setTimezone(new \DateTimeZone('UTC'));
        $start_date = $from ?? $dateStart;
        $end_date = $to ?? $date;

        $request = new FindItemType();
        $request->ParentFolderIds = new NonEmptyArrayOfBaseFolderIdsType();

        // Build the start date restriction.
        $greater_than = new IsGreaterThanOrEqualToType();
        $greater_than->FieldURI = new PathToUnindexedFieldType();
        $greater_than->FieldURI->FieldURI = UnindexedFieldURIType::ITEM_DATE_TIME_RECEIVED;
        $greater_than->FieldURIOrConstant = new FieldURIOrConstantType();
        $greater_than->FieldURIOrConstant->Constant = new ConstantValueType();
        $greater_than->FieldURIOrConstant->Constant->Value = $start_date->format('c');

        // Build the end date restriction;
        $less_than = new IsLessThanOrEqualToType();
        $less_than->FieldURI = new PathToUnindexedFieldType();
        $less_than->FieldURI->FieldURI = UnindexedFieldURIType::ITEM_DATE_TIME_RECEIVED;
        $less_than->FieldURIOrConstant = new FieldURIOrConstantType();
        $less_than->FieldURIOrConstant->Constant = new ConstantValueType();
        $less_than->FieldURIOrConstant->Constant->Value = $end_date->format('c');

        // Build the restriction.
        $request->Restriction = new RestrictionType();
        $request->Restriction->And = new AndType();
        $request->Restriction->And->IsGreaterThanOrEqualTo = $greater_than;
        $request->Restriction->And->IsLessThanOrEqualTo = $less_than;

        // Return all message properties.
        $request->ItemShape = new ItemResponseShapeType();
        $request->ItemShape->BaseShape = DefaultShapeNamesType::ALL_PROPERTIES;

        // Search in the user's inbox.
        $folder_id = new DistinguishedFolderIdType();
        $folder_id->Id = DistinguishedFolderIdNameType::INBOX;
        $request->ParentFolderIds->DistinguishedFolderId[] = $folder_id;
        return $this->client->FindItem($request);
    }
}
