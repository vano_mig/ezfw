<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\db\Query;

class Flights extends ActiveRecord
{
    public static function tableName()
    {
        return 'flights';
    }

    public static function getModelById($id)
    {
        return self::findOne($id);
    }

    public static function getModelByFlightId($id)
    {
        return self::findOne(['flights_schedule_id'=>$id]);
    }

    public static function getFlightByParams($obj)
    {

        return self::findOne(['flight_number'=>$obj->flightNumber, 'departure_time'=>$obj->departureTime.' 00:00:00']);
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord) {
            $this->created_at = date('Y-m-d H:i:s');
            $this->updated_at = date('Y-m-d H:i:s');
        } else {
            $this->updated_at = date('Y-m-d H:i:s');
        }
        return parent::beforeSave($insert);
    }

    public static function getFlightsByDate($params)
    {
        $model = new Query();
        $model->select(['id', 'flight_number', 'flights_schedule_id', 'departure_time', 'reg_number', 'departure_airport', 'arrival_airport']);
        $model->from('flights');
        if(!empty($params['date_from']))
            $model->andWhere(['>=', 'departure_time', $params['date_from']]);
        if(!empty($params['date_to']))
            $model->andWhere(['<=', 'departure_time', $params['date_to']]);

        $result = $model->all();

        return $result;
    }
}
