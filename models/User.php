<?php

namespace app\models;

use app\models\form\UserForm;
use Yii;
use yii\bootstrap4\Html;
use yii\db\ActiveRecord;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
    public static function tableName()
    {
        return 'users';
    }
    /**
     * @param $email
     * @return User|false
     */
    public static function getUserByEmail($email)
    {
        $user = self::find()->where(['email' => $email])->andWhere(['active' => '1'])->one();
        if ($user) return $user;
        return false;
    }
    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return self::getUserById($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        $user = self::find()->where(['name' => $username])->one();

        return $user ?? null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }

    public static function isExists($email)
    {
        $user = self::find()->where(['email' => $email])->one();
        if ($user) return true;
        return false;
    }

    public static function getUserById($user_id)
    {
        $user = self::find()->where(['id' => $user_id])->one();
        if ($user) return $user;
        return false;
    }

    public static function getUserByUserID($id) {
        $user = self::getUserById($id);
        if ($user) {
            $model = new UserForm();
            $model->userId = $user->id;
            $model->name = $user->name;
            $model->surname = $user->surname;
            $model->email = $user->email;
            $model->company = $user->company;
            $model->isAdmin = $user->is_admin;
            $model->isActive = $user->active;
            return $model;
        }
        return false;
    }

    public function checkPassword($password)
    {
        if (!$this->password)
            return false;

        if (\Yii::$app->getSecurity()->validatePassword($password, $this->password)) {
            return true;
        } else {
            return false;
        }
    }

    public function checkActive()
    {
        if ($this->active == 1)
            return true;

        return false;
    }

    public static function getList($limit = false, $offset = false, $count = false)
    {
        $models = self::find()->orderBy(['id'=>SORT_DESC]);
        if ($limit !== false && $offset !== false) {
            $models->limit($limit);
            $models->offset($offset);
        }
        if($count)
            return $models->count();

        return $models->all();
    }

    public function getIsAdmin()
    {
        $format = Html::tag('span', 'No', ['class'=>'badge badge-primary']);

        if($this->is_admin == 'Y')
            $format = Html::tag('span', 'Yes', ['class'=>'badge badge-success']);
        return $format;

    }

    public function getIsActive()
    {
        $format = Html::tag('span', 'Active', ['class'=>'badge badge-success']);
        if(!$this->active)
            $format = Html::tag('span', 'Not active', ['class'=>'badge badge-danger']);
        return $format;

    }

    public static function saveUser($user)
    {
        $result = false;
        $model = null;

        if($user->userId) {
            $model = self::findOne($user->userId);
        }

        if(!$model)
            $model = new User();

        $model->name = $user->name;
        $model->surname = $user->surname;
        $model->email = $user->email;

        if(!$user->userId || ($model->id && $user->password)) {
            $model->password = Yii::$app->getSecurity()->generatePasswordHash($user->password);
        }
        $model->company = $user->company;
        $model->is_admin = $user->isAdmin;
        $model->active = $user->isActive;
        if($model->save()) {
            $result = true;
            UserLogbook::log("save user `{$model->id}` name `{$model->name}` email `{$model->email}`");
        }

        return $result;
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord) {
            $this->created_at = date('Y-m-d H:i:s');
            $this->updated_at = date('Y-m-d H:i:s');
        }else {
            $this->updated_at = date('Y-m-d H:i:s');
        }
        return parent::beforeSave($insert);
    }
}
