<?php

namespace app\models;

use yii\db\ActiveRecord;

class FlightDetails extends ActiveRecord
{
    public static function tableName()
    {
        return 'flights_details';
    }

    public static function getModelById($id)
    {
        return self::findOne($id);
    }

    public static function getModelByFlightId($id)
    {
        return self::findOne(['flight_id'=>$id]);
    }
}
