<?php

namespace app\models;

use Yii;
use yii\web\HttpException;

class Auth
{

    /**
     * Login method
     * @param $email
     * @param $password
     * @throws \Exception
     * @return bool
     */
    public static function login($email, $password)
    {
        $email = strtolower($email);

        $session = Yii::$app->session;
        if (!$session->isActive)
            $session->open();

        $user = User::getUserByEmail($email);
        if (!$user) return false;

        if ($user->checkPassword($password)){
            $session->set('auth_user', $user);
            if ($user->checkActive()) {
                UserLogbook::log("User `{$user->email}` log out to system ");
                return true;
            } else {
                UserLogbook::log("User `{$user->email}` is not active ");
            }
        } else {
            $session->set('auth_user', false);
        }

        return false;
    }

    public static function logout()
    {
        $session = Yii::$app->session;
        if (!$session->isActive)
            $session->open();

        $user = $session->get('auth_user', false);

        if ($user) {
            UserLogbook::log("User `{$user->email}` log out to system ");
        }

        Yii::$app->session->destroy();

        return true;
    }

    public static function getLoggedUser()
    {
        if (!isset(Yii::$app->session))
            return false;

        $session = Yii::$app->session;
        if (!$session) return false;
        if (!$session->isActive)
            $session->open();

        $user = $session->get('auth_user', false);

        if ($user) {
            //reload user to session
            $user = User::getUserById($user->id);
            $session->set('auth_user', $user);
        }

        return $user;
    }

    public static function isAdmin()
    {
        $loggedUser = static::getLoggedUser();
        if (!$loggedUser) return 'N';
        return $loggedUser->is_admin;
    }
}