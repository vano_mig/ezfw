<?php

namespace app\models\form;

use app\models\FlightDetails;
use app\models\Flights;
use app\models\RequestsLogs;
use DateTime;
use Yii;
use yii\base\Model;

class FlightDetailModel extends Model
{
    public $id;
    public $flightId;
    public $totalMale;
    public $totalFemale;
    public $totalChild;
    public $totalInfant;
    public $averageBaggageWeight;
    public $cargoWeight;
    public $noShow;
    public $czwf;
    public $ezwf;
    public $dow;
    public $azwf;
    public $note;

    public static function getFlightByID($id)
    {
        $model = FlightDetails::getModelById($id);
        if($model === null)
            return false;
        $objFlight = new FlightDetailModel();
        $objFlight->id = $model->id;
        $objFlight->flightId = $model->flight_id;
        $objFlight->totalMale = $model->total_male;
        $objFlight->totalFemale = $model->total_female;
        $objFlight->totalChild = $model->total_child;
        $objFlight->totalInfant = $model->total_infant;
        $objFlight->averageBaggageWeight = $model->average_baggage_weight;
        $objFlight->cargoWeight = $model->cargo_weight;
        $objFlight->noShow = $model->no_show;
        $objFlight->czwf = $model->czwf;
        $objFlight->ezwf = $model->ezwf;
        $objFlight->dow = $model->dow;
        $objFlight->azwf = $model->azwf;
        $objFlight->note = $model->note;

        return $objFlight;
    }

    public static function getFlightByFlightID($id)
    {
        $model = FlightDetails::getModelByFlightId($id);
        if($model === null)
            return false;
        $objFlight = new FlightDetailModel();
        $objFlight->id = $model->id;
        $objFlight->flightId = $model->flight_id;
        $objFlight->totalMale = $model->total_male;
        $objFlight->totalFemale = $model->total_female;
        $objFlight->totalChild = $model->total_child;
        $objFlight->totalInfant = $model->total_infant;
        $objFlight->averageBaggageWeight = $model->average_baggage_weight;
        $objFlight->cargoWeight = $model->cargo_weight;
        $objFlight->noShow = $model->no_show;
        $objFlight->czwf = $model->czwf;
        $objFlight->ezwf = $model->ezwf;
        $objFlight->dow = $model->dow;
        $objFlight->azwf = $model->azwf;
        $objFlight->note = $model->note;

        return $objFlight;
    }


    public static function saveFlight($flidhtObj)
    {
        $model = FlightDetails::getModelByFlightId($flidhtObj->flightId);

        if($model === null)
            $model = new FlightDetails();

        $model->flight_id = $flidhtObj->flightId;
        $model->total_male = $flidhtObj->totalMale;
        $model->total_female = $flidhtObj->totalFemale;
        $model->total_child = $flidhtObj->totalChild;
        $model->total_infant = $flidhtObj->totalInfant;
        $model->average_baggage_weight = $flidhtObj->averageBaggageWeight;
        $model->cargo_weight = $flidhtObj->cargoWeight;
        $model->no_show = $flidhtObj->noShow;
        $model->czwf = $flidhtObj->czwf;
        $model->ezwf = $flidhtObj->ezwf;
        $model->dow = $flidhtObj->dow;
        $model->azwf = $flidhtObj->azwf;
        $model->note = $flidhtObj->note;
        $model->save();
    }

    public static function getRegNumber($url, $params)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        if (!empty($result)) {
            $result = json_decode($result, true);
        }
        if (isset($result["response"]["result"]["code"]) &&  $result["response"]["result"]["code"] == "0" && !empty($result["response"]["flight"]["id"])) {

            return $result["response"]["flight"]["id"];
        }
        return false;
    }

    public static function getPnl($params)
    {
        $url = Yii::$app->params['inventor_url'] . '/v2.0/pnl/get?' . http_build_query($params);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        if (!empty($result)) {
            $result = json_decode($result, true);
        }
        return $result;
    }
}