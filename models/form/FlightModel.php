<?php

namespace app\models\form;

use app\models\Flights;
use DateTime;
use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\debug\panels\EventPanel;

class FlightModel extends Model
{
    public const MALE_WEIGHT = 88;
    public const FEMALE_WEIGHT = 70;
    public const CHILD_WEIGHT = 35;
    public const NO_SHOW_WEIGHT = 94;

    public $flightId;
    public $flightNumber;
    public $flightScheduleId;
    public $regNumber;
    public $departureAirport;
    public $departureTime;
    public $departureStd;
    public $arrivalAirport;
    public $arrivalTime;
    public $arrivalSta;

    public static function getFlightByID($id)
    {
        $model = Flights::getModelById($id);
        if($model === null)
            return false;
        $objFlight = new FlightModel();
        $objFlight->flightId = $model->id;
        $objFlight->flightNumber = $model->flight_number;
        $objFlight->flightScheduleId = $model->flights_schedule_id;
        $objFlight->regNumber = $model->reg_number;
        $objFlight->departureAirport = $model->departure_airport;
        $objFlight->departureTime = $model->departure_time;
        $objFlight->departureStd = $model->departure_std;
        $objFlight->arrivalAirport = $model->arrival_airport;
        $objFlight->arrivalTime = $model->arrival_time;
        $objFlight->arrivalSta = $model->arrival_sta;

        return $objFlight;
    }

    public static function getFlightByFlightID($id)
    {
        $model = Flights::getModelByFlightId($id);
        if($model === null)
            return false;
        $objFlight = new FlightModel();
        $objFlight->flightId = $model->id;
        $objFlight->flightNumber = $model->flight_number;
        $objFlight->flightScheduleId = $model->flights_schedule_id;
        $objFlight->regNumber = $model->reg_number;
        $objFlight->departureAirport = $model->departure_airport;
        $objFlight->departureTime = $model->departure_time;
        $objFlight->departureStd = $model->departure_std;
        $objFlight->arrivalAirport = $model->arrival_airport;
        $objFlight->arrivalTime = $model->arrivale_time;
        $objFlight->arrivalSta = $model->arrival_sta;

        return $objFlight;
    }


    public static function saveFlight($flidhtObj)
    {
        $model = Flights::getFlightByParams($flidhtObj);

        if($model === null)
            $model = new Flights();

        $model->flight_number = $flidhtObj->flightNumber;
        $model->reg_number = $flidhtObj->regNumber;
        $model->departure_airport = $flidhtObj->departureAirport;
        $model->departure_time = $flidhtObj->departureTime;
        $model->departure_std = $flidhtObj->departureStd;
        $model->arrival_airport = $flidhtObj->arrivalAirport;
        $model->arrival_time = $flidhtObj->arrivalTime;
        $model->arrival_sta = $flidhtObj->arrivalSta;
        $model->flights_schedule_id = $flidhtObj->flightScheduleId;
        $model->save();
    }

    public static function getPassengers($search = false)
    {

        $params = [];
        if($search) {
            $date = new DateTime(date('Y-m-d H:i:s'));
            $date->setTimezone(new \DateTimeZone('UTC'));
            $date->modify('-24 hour');
            $departureDate = $date->format('Y-m-d');
            $std = $date->format('H:i');
            $params = [
                'date_from' => $departureDate,
                'std_from' => $std,
            ];

            $date = new DateTime(date('Y-m-d H:i:s'));
            $date->setTimezone(new \DateTimeZone('UTC'));
            $date->modify('+48 hour');
            $departureDate = $date->format('Y-m-d');
            $std = $date->format('H:i');
            $params['date_to'] = $departureDate;
            $params['std_to'] = $std;
        }
        $result = Flights::getFlightsByDate($params);

        return $result;
    }

    public static function getFlightList($search, $limit, $offset, $count = false)
    {
        $sql = new Query();
        if($count)
            $sql->select( ['COUNT(*)']);
        else
            $sql->select(['f.id', 'f.flight_number', 'f.reg_number', 'f.flights_schedule_id', 'f.departure_time', 'departure_std', 'arrival_time', 'arrival_sta',
                'fd.total_male', 'fd.total_female', 'fd.total_child', 'fd.total_infant', 'fd.average_baggage_weight', 'fd.no_show',
                'fd.cargo_weight', 'fd.czwf', 'fd.ezwf', 'fd.dow', 'fd.azwf']);
        $sql->from('flights as f');
        $sql->leftJoin('flights_details as fd', 'f.id = fd.flight_id');
        if(!empty($search['date']))
            $sql->andWhere(['f.departure_time'=>$search['date']]);
        if(!empty($search['flight']))
            $sql->andWhere(['f.flight_number'=>$search['flight']]);
        if(!empty($search['reg_number']))
            $sql->andWhere(['f.reg_number'=>$search['reg_number']]);
        if(!empty($search['diff'])) {
            if($search['diff'] == 'ok') {
                $sql->andWhere(['f.reg_number'=>$search['reg_number']]);
            } else {
                $sql->andWhere(['f.reg_number'=>$search['reg_number']]);
            }
        }
        if(!$count) {
            $sql->limit($limit);
            $sql->offset($offset);
            $sql->orderBy(['departure_time'=>SORT_ASC]);
            $result = $sql->all();
            $result = self::calculation($result);
        } else {
            $result = $sql->count();
        }

        return $result;
    }

    public static function calculation($query)
    {
        $result = [];
        if(empty($query))
            return $result;
        foreach ($query as $item) {
            $weight = (int)$item['total_male'] * self::MALE_WEIGHT + (int)$item['total_female'] * self::FEMALE_WEIGHT
                + (int)$item['total_child'] * self::CHILD_WEIGHT +
                (((int)$item['total_male'] + (int)$item['total_female']
                        + (int)$item['total_child']) * (int)$item['average_baggage_weight'])
                + (int)$item['cargo_weight'] - (int)$item['no_show'] * self::NO_SHOW_WEIGHT;
            $item = [
                'id'=>$item['id'],
                'date'=>date('dMY', strtotime($item['departure_time'])),
                'flight_number'=>$item['flight_number'],
                'reg_number'=>$item['reg_number'],
                'departure_std'=>$item['departure_std'],
                'total_male'=> (int)$item['total_male'],
                'total_female'=> (int)$item['total_female'],
                'total_child'=> (int)$item['total_child'],
                'total_infant'=> (int)$item['total_infant'],
                'estimate'=> number_format($weight, 2, '.', ''),
                'czwf'=> $item['czwf'],
                'ezwf'=> $item['ezwf'],
                'diff'=> '',
                'azwf'=> $item['azwf'],
                ];
            $result[] = $item;
        }
        return $result;
    }
}