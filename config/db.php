<?php

$db =  [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=ezfw',
    'username' => 'joinup',
    'password' => 'GlfqUwkV',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];

if (file_exists(__DIR__ . '/db.local.php')) {
    $paramLocal = require __DIR__ . '/db.local.php';
    foreach ($paramLocal as $name => $value) {
        $db[$name] = $value;
    }
}

return $db;
