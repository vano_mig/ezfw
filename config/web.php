<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'Lxgw8PbRdDmPTktHMWZkxK1ir31Ch3Sm',
            'enableCsrfValidation' => false,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'boundstate\mailgun\Mailer',
            'key' => 'key-8cd4ca33fd44134fcf34467ccae9627c',
            'domain' => 'skyup.aero',
        ],
        'log' => [
            'traceLevel' => 3,
            'targets' => [],
        ],
        'turbosms' => [
            'class' => 'avator\turbosms\Turbosms',
            'sender' => 'SkyUp',
            'login' => 'joinsoapsmsu',
            'password' => 'joinsoapsmsp',
            'debug' => false,
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/'                                     => '/site/login',
                '/login'                                => '/site/login',
                '/logout'                               => '/site/logout',
                '/forgot-password'                      =>'/site/forgot-password',
                '/show'                                 => '/settings/show',
                '/users'                                =>'/settings/users',
                '/new-user'                             =>'/settings/create',
                '/save-user'                            =>'/settings/save-user',
                '/update-user/<id>'                     =>'/settings/update-user',
                '/update-route/<id>'                    =>'/settings/update-route',
                '/no-show-base'                         =>'/settings/no-show-base',
                '/new-route'                            =>'/settings/new-route',
                '/save-route'                            =>'/settings/save-route',

            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV == "dev") {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

if (YII_ENV == "prod") {
    $config['components']['log']['targets']['telegram'] = [
        'class' => 'app\components\TargetHelper',
        'levels' => ['error', 'warning'],
        'except' => [
            'yii\web\HttpException:404',
            'yii\web\HttpException:403',
            'yii\web\HttpException:401', // yii\web\UnauthorizedHttpException: Your request was made with invalid credentials
            'yii\web\HttpException:400', //yii\web\BadRequestHttpException: Не удалось проверить переданные данные.
        ],
        'token' => '1303338455:AAEQtjvi6A4qqUhbuHdHxnaUmz4dTa8JD6k',
        'chatId' => -1001365942418,
        'logVars' => [],
        'template' => "{levelAndRequest}\n{text}\n{category}\n{user}\n{stackTrace}\n\nENV: ".YII_ENV,
    ];
}

return $config;
