<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <?php $this->registerCsrfMetaTags() ?>
    <title>SkyUp Airlines</title>

    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/favicon/site.webmanifest">
    <link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#ff6b00">
    <meta name="msapplication-TileColor" content="#ff6b00">
    <meta name="theme-color" content="#000000">
    <meta name="msapplication-navbutton-color" content="#000000">
    <meta name="apple-mobile-web-app-status-bar-style" content="#000000">

    <script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>


    <link href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" rel="stylesheet">
    <link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">

    <script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <link href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
    <?php $this->head() ?>
</head>
<body>
<header id="header" class="header">
    <div class="header__unit header__left">
        <a class="header__logo-link header__item" href="/">
            <svg class="icon icon-logo-text">
                <use xlink:href="/img/sprite.svg?v=27#icon-logo-text"></use>
            </svg>
        </a>
    </div>
    <div class="header__right">
        <?php if (\app\models\Auth::isAdmin() == 'Y'): ?>
            <a href="/show" data-toggle="tooltip" data-placement="bottom" title="Flight list"><i class="fas fa-plane"></i></a>
            <div class="header-separator"></div>
            <a href="/users" data-toggle="tooltip" data-placement="bottom" title="Manage users"><i class="fas fa-users-cog"></i></a>
            <div class="header-separator"></div>
            <a href="/no-show-base" data-toggle="tooltip" data-placement="bottom" title="Manage DataBase"><i class="far fa-credit-card"></i></a>
            <div class="header-separator"></div>
        <?php endif ?>
    </div>
</header>
<?php $this->beginBody() ?>
<?= $content ?>
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
