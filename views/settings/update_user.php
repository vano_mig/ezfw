<?php
use yii\bootstrap4\ActiveForm;
?>

<div class="content">
    <div class="container">
        <div class="content-name mb-3"><i class="fas fa-users"></i> Manage Users</div>
        <div class="content-body">
            <div id="flights">
                <div class="header-form pt-4 mb-2">Update user <?="$user->name / $user->surname"?></div>
                <?php $form = ActiveForm::begin([
                    'id' => 'user-form',
                    'action'=>'/save-user',
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-4',
                            'offset' => 'offset-sm-4',
                            'wrapper' => 'col-sm-8',
                            'error' => '',
                        ],
                    ],
                ]) ?>

                <?=$form->field($user, 'userId')->hiddenInput()->label(false)?>
                <?=$form->field($user, 'scenario')->hiddenInput(['value'=>'update'])->label(false)?>
                <?=$form->field($user, 'name')->textInput()?>
                <?=$form->field($user, 'surname')->textInput()?>
                <?=$form->field($user, 'email')->input('email')?>
                <?=$form->field($user, 'company')->textInput()?>
                <?=$form->field($user, 'isAdmin')->dropdownList(['N'=>'No', 'Y'=>'Yes'], ['placeholder'=>'Please select role'])?>
                <?=$form->field($user, 'isActive')->dropdownList(['0'=>'Not active', '1'=>'Active'], ['placeholder'=>'Please select status'])?>
                <?=$form->field($user, 'password')->passwordInput()?>
                <div class="row form-buttons">
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-success btn-block">Save</button>
                    </div>
                    <div class="col-sm-6">
                        <a href="/users" class="btn btn-primary btn-block">Cancel</a>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>