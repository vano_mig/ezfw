<div class="content">
    <div class="container">
        <div class="content-name mb-3"><i class="fas fa-route"></i> Routes list</div>
        <div class="content-body">
            <div id="flights">
                <div class="float-right mb-4 mt-2 mr-2">
                    <a href="/new-route" class="btn btn-sm btn-success">New route</a>
                </div>
                <table class="table table-bordered table-striped table-flights">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Average Baggage Weight</th>
                        <th>No Show</th>
                        <th>Note</th>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($routeList as $route) : ?>
                        <tr>
                            <td><?= $route->id ?></td>
                            <td><?= $route->from ?></td>
                            <td><?= $route->to ?></td>
                            <td><?= $route->average_baggage_weight ?></td>
                            <td><?= $route->no_show ?></td>
                            <td><?= $route->note ?></td>
                            <td>
                                <a href="/update-route/<?= $route->id ?>" title="Update"><i class="fas fa-user-edit"></i>
                                <a href="/delete-route/<?= $route->id ?>" title="Delete"
                                    data-confirm="Are you sure you want to delete?"
                                    data-method="post" data-pjax="0"><i class="fas fa-trash-alt"></i>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach;

                    use app\widgets\Pagination; ?>
                    </tbody>
                </table>
                <?= Pagination::widget([
                    'page' => $pagination['currentPage'],
                    'pages' => $pagination['pages'],
                    'generator' => function ($nPage) use ($filter) {
                        $filter['page'] = $nPage;

                        return '?' . http_build_query($filter);
                    }
                ]); ?>
            </div>
        </div>
    </div>
</div>