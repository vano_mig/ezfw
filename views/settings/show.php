<?php

use app\widgets\Pagination;
use yii\jui\DatePicker;

?>
<div class="content">
    <div class="container">
        <div class="content-name mb-3"><i class="fas fa-plane"></i> Flights list</div>
        <div class="content-body">
            <div id="flights">
                <div class=" filter-flights">
                    <form method="get">
                        <div class="d-flex justify-content-center">
                            <input type="text" placeholder="flight number" class="filter-field" name="search[flight]" value="<?=empty($search['flight']) ? '': $search['flight']?>">
                            <input type="text" placeholder="reg number" class="filter-field" name="search[reg_number]" value="<?=empty($search['reg_number']) ? '': $search['reg_number']?>">
                            <?= DatePicker::widget(['name' => 'search[date]', 'dateFormat' => 'yyyy-MM-dd',
                            'value'=>empty($search['date']) ? '': $search['date'], 'options'=>['class'=>'filter-field', 'placeholder'=>'Input date', ]]) ?>
                            <select class="filter-field" name="search[diff]">
                                <option value="">DIFF</option>
                                <option value="ok" <?=empty($search) ? '' : $search['diff'] == 'ok' ? 'selected': ''?>>OK</option>
                                <option value="error" <?=empty($search) ? '' : $search['diff'] == 'error' ? 'selected': ''?>>Error</option>
                            </select>
                            <button type="submit" class="btn btn-warning filter-submit">Search</button>
                    </form>
                </div>
                <table class="table table-bordered table-striped table-flights">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Date</th>
                        <th>Flight number</th>
                        <th>Reg. number</th>
                        <th>STD</th>
                        <th>Total Male</th>
                        <th>Total Female</th>
                        <th>Total Child</th>
                        <th>Total INF</th>
                        <th>Estimated payload</th>
                        <th>CZFW</th>
                        <th>EZFW</th>
                        <th>Diff.</th>
                        <th>AZFW</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($flights as $flight):?>
                    <tr>
                        <td><?=$flight['id']?></td>
                        <td><?=$flight['date']?></td>
                        <td><?=$flight['flight_number']?></td>
                        <td><?=$flight['reg_number']?></td>
                        <td><?=$flight['departure_std']?></td>
                        <td><?=$flight['total_male']?></td>
                        <td><?=$flight['total_female']?></td>
                        <td><?=$flight['total_child']?></td>
                        <td><?=$flight['total_infant']?></td>
                        <td><?=$flight['estimate']?></td>
                        <td><?=$flight['czwf']?></td>
                        <td><?=$flight['ezwf']?></td>
                        <td><?=$flight['diff']?></td>
                        <td><?=$flight['azwf']?></td>
                    </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>
                <?= Pagination::widget([
                    'page' => $pagination['currentPage'],
                    'pages' => $pagination['pages'],
                    'generator' => function ($nPage) use ($filter) {
                        $filter['page'] = $nPage;

                        return '?' . http_build_query($filter);
                    }
                ]); ?>
                <br/>
            </div>
        </div>
    </div>
</div>