<div class="content">
    <div class="container">
        <div class="content-name mb-3"><i class="fas fa-users"></i> User list</div>
        <div class="content-body">
            <div id="flights">
                <div class="float-right mb-4 mt-2 mr-2">
                    <a href="/new-user" class="btn btn-sm btn-success">New User</a>
                </div>
                <table class="table table-bordered table-striped table-flights">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Company</th>
                        <th>Is Admin</th>
                        <th>Status</th>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($userList as $user) : ?>
                        <tr>
                            <td><?= $user->id ?></td>
                            <td><?= $user->name ?></td>
                            <td><?= $user->surname ?></td>
                            <td><?= $user->email ?></td>
                            <td><?= $user->company ?></td>
                            <td><?= $user->getIsAdmin() ?></td>
                            <td><?= $user->getIsActive() ?></td>
                            <td>
                                <a href="/update-user/<?= $user->id ?>" title="Update"><i class="fas fa-user-edit"></i>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach;

                    use app\widgets\Pagination; ?>
                    </tbody>
                </table>
                <?= Pagination::widget([
                    'page' => $pagination['currentPage'],
                    'pages' => $pagination['pages'],
                    'generator' => function ($nPage) use ($filter) {
                        $filter['page'] = $nPage;

                        return '?' . http_build_query($filter);
                    }
                ]); ?>
            </div>
        </div>
    </div>
</div>