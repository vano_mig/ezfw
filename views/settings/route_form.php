<?php
use yii\bootstrap4\ActiveForm;
?>

<div class="content">
    <div class="container">
        <div class="content-name mb-3"><i class="fas fa-route"></i> Manage Routes</div>
        <div class="content-body">
            <div id="flights">
                <div class="header-form pt-4 mb-2">New route</div>
                <?php $form = ActiveForm::begin([
                    'id' => 'route-form',
                    'action'=>'/save-route',
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-4',
                            'offset' => 'offset-sm-4',
                            'wrapper' => 'col-sm-8',
                            'error' => '',
                        ],
                    ],
                ]) ?>
                <?=$form->field($route, 'from')->textInput()->label('Departure')?>
                <?=$form->field($route, 'to')->textInput()->label('Arrival')?>
                <?=$form->field($route, 'averageBaggageWeight')->textInput()?>
                <?=$form->field($route, 'noShow')->textInput()?>
                <?=$form->field($route, 'note')->textInput()?>
                <div class="row form-buttons">
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-success btn-block">Save</button>
                    </div>
                    <div class="col-sm-6">
                        <a href="/no-show-base" class="btn btn-primary btn-block">Cancel</a>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>