<?php
    $this->title = "SkyUp Agent";
    $this->registerCssFile('/css/login.css');

?>

<section class="login">
    <form class="login__form" method="POST" action="/forgot-password">
        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />

        <h1 class="login__logo">
            <img src="/img/img__logo.svg" alt="SkyUp Airlines" width="218" height="40">
        </h1>

        <?php if (isset($status)): ?>
            <?php if ($status === false): ?>
                <p class="error-box" style="color: red">Wrong email or password</p>
            <?php else: ?>
                <p class="success-box" style="color: green">The new password will sent to your Email address</p>
            <?php endif ?>
        <?php endif ?>

        <label class="login__item">
            <input class="login__input" type="text" name="LoginForm[email]" required="" placeholder="...">
            <span class="login__label">Email</span>
        </label>

        <a href="/login" class="forgot-link">Back to authorization</a>

        <div class="login__submit">
            <button class="login__btn" type="submit">Get password</button>
        </div>

    </form>
</section>
