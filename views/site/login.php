
<!-- LOGIN -->
<section class="login">
    <form class="login__form" method="POST" action="/">
        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />

        <h1 class="login__logo">
            <img src="/img/img__logo.svg" alt="SkyUp Airlines" width="218" height="40">
        </h1>

        <label class="login__item">
            <input class="login__input" type="text" name="email" required="" placeholder="...">
            <span class="login__label">Email</span>
        </label>

        <label class="login__item">
            <input class="login__input" type="password" name="password" required="" placeholder="...">
            <span class="login__label">Password</span>
        </label>
        <a href="/forgot-password" class="forgot-link">Lost password?</a>

        <div class="login__submit">
            <button class="login__btn" type="submit">Log in</button>
        </div>

        <?php if ($error): ?>
            <p class="login__alarm">Неверный логин или пароль</p>
        <?php endif ?>
    </form>
</section>